import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
class ListPageEvent extends Equatable {
  @override
  List<Object> get props => [];
}

@immutable
class ListPageEventLoad extends ListPageEvent {}
