<img src="images/logo.png" alt="CryptoList"/>

# CryptoList

A Flutter application to show cryptocurrency prices.

If you want to clone this project create a [CoinAPI](https://rest.coinapi.io) API key and set it to `_apiKey` variable in file `data/remote/service.dart`.

If you press cryptocurrency icon in it's dedicated page it will search google about it.

<img src="images/list_page_1.png" alt="list page 1" width="50%" /><img src="images/list_page_2.png" alt="list page 2" width="50%" />

<img src="images/list_page_filter_1.png" alt="list page filter 1" width="50%" /><img src="images/list_page_filter_2.png" alt="list page filter 2" width="50%" />

<img src="images/crypto_page_loading.png" alt="crypto page loading" width="50%" /><img src="images/crypto_page.png" alt="crypto page" width="50%" />
